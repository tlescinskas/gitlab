terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}

provider "gitlab" {
  token = chomp(file("~/.gitlab/token"))
}


resource "gitlab_project_variable" "very_important_secret" {
  project   = "56091716"
  key       = "secret"
  value     = "We are protecting the protectables!"
  protected = false
}
