output "instance" {
  value = aws_instance.nginx.public_ip
}

output "web" {
  value = "http://${aws_instance.nginx.public_ip}"
}
