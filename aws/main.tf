provider "aws" {
  profile = "devops"
  region  = "us-east-1"
}

data "aws_vpc" "main" {
  id = "vpc-5266df2f"
}

data "aws_subnet" "main" {
  id = "subnet-33e48f12"
}

resource "aws_instance" "nginx" {
  ami                         = data.aws_ami.main.id
  instance_type               = "t3a.micro"
  subnet_id                   = data.aws_subnet.main.id
  ebs_optimized               = true
  vpc_security_group_ids      = [aws_security_group.nginx.id]
  user_data_replace_on_change = true
  user_data                   = <<EOF
#!/bin/bash
apt update -y
apt install nginx -y

EOF
}

resource "aws_security_group" "nginx" {
  name        = "nginx-whitelist"
  description = "Nginx security group"
  vpc_id      = data.aws_vpc.main.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #   ingress {
  #     from_port   = 22
  #     to_port     = 22
  #     protocol    = "tcp"
  #     cidr_blocks = ["0.0.0.0/0"]
  #   }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}





data "aws_ami" "main" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}
